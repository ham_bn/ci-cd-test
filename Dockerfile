FROM node
WORKDIR /app
ADD . /app
RUN npm install
CMD ["node", "app.js"]